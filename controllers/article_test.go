package controllers

import (
	"blogs/models"
	"encoding/json"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

var (
	rightData = `{"title":"Hello World","content":"Lorem ipsum dolor sit amet.","author":"Jack"}`
	wrongData = `{"title":"Hello World"}`
)

var r = gin.Default()

func init() {
	models.ConnectDataBase()

	r.POST("/articles", CreateArticle)
	r.GET("/articles/:id", GetArticle)
	r.GET("/articles", GetArticles)
}

func TestCreateArticle(t *testing.T) {
	req := httptest.NewRequest("POST", "/articles", strings.NewReader(rightData))
	resp := httptest.NewRecorder()
	r.ServeHTTP(resp, req)
	assert.Equal(t, 201, resp.Code)

	data := CreateArticleResp{}
	_ = json.Unmarshal([]byte(resp.Body.Bytes()), &data)
	var article models.Article
	models.DB.First(&article, data.ID)
	models.DB.Delete(&article)

	req = httptest.NewRequest("POST", "/articles", strings.NewReader(wrongData))
	resp = httptest.NewRecorder()
	r.ServeHTTP(resp, req)
	assert.Equal(t, 400, resp.Code)
}

func TestGetArticle(t *testing.T) {
	var article models.Article
	err := models.DB.First(&article, 1).Error
	if err != nil {
		testData := models.Article{
			ID:      1,
			Title:   "Hello",
			Content: "Hello world",
			Author:  "world",
		}
		models.DB.Create(&testData)
	}

	req := httptest.NewRequest("GET", "/articles/1", nil)
	resp := httptest.NewRecorder()
	r.ServeHTTP(resp, req)
	assert.Equal(t, 200, resp.Code)

	if err != nil {
		models.DB.Delete(&article)
	}

	req = httptest.NewRequest("GET", "/articles/0", nil)
	resp = httptest.NewRecorder()
	r.ServeHTTP(resp, req)
	assert.Equal(t, 404, resp.Code)
}

func TestGetArticles(t *testing.T) {
	models.ConnectDataBase()
	req := httptest.NewRequest("GET", "/articles", nil)
	resp := httptest.NewRecorder()
	r.ServeHTTP(resp, req)
	assert.Equal(t, 200, resp.Code)
}
