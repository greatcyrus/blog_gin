package controllers

import (
	"blogs/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

type CreateArticleReq struct {
	Title   string `json:"title" binding:"required"`
	Content string `json:"content" binding:"required"`
	Author  string `json:"author" binding:"required"`
}

type CreateArticleResp struct {
	ID int `json:"id"`
}

func CreateArticle(c *gin.Context) {
	var req CreateArticleReq
	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(
			http.StatusBadRequest,
			gin.H{
				"status":  http.StatusBadRequest,
				"message": err.Error(),
				"data":    nil,
			},
		)
		return
	}
	article := models.Article{
		Title:   req.Title,
		Content: req.Content,
		Author:  req.Author,
	}
	models.DB.Create(&article)
	resp := CreateArticleResp{
		ID: article.ID,
	}
	c.JSON(
		http.StatusCreated,
		gin.H{
			"status":  http.StatusCreated,
			"message": "Success",
			"data":    resp,
		},
	)
}

func GetArticle(c *gin.Context) {
	var article models.Article
	err := models.DB.Where("id = ?", c.Param("id")).First(&article).Error
	if err != nil {
		c.JSON(
			http.StatusNotFound,
			gin.H{
				"status":  http.StatusNotFound,
				"message": "Not found",
				"data":    nil,
			},
		)
		return
	}
	c.JSON(
		http.StatusOK,
		gin.H{
			"status":  http.StatusOK,
			"message": "Success",
			"data":    article,
		},
	)
}

func GetArticles(c *gin.Context) {
	var articles []models.Article
	models.DB.Find(&articles)
	c.JSON(
		http.StatusOK,
		gin.H{
			"status":  http.StatusOK,
			"message": "Success",
			"data":    articles,
		},
	)
}
