package models

import (
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var DB *gorm.DB

func ConnectDataBase() {
	db, err := gorm.Open("mysql", os.Getenv("DB_USER")+":"+os.Getenv("DB_PASS")+"@("+os.Getenv("DB_HOST")+")/blog?charset=utf8mb4")
	if err != nil {
		panic("Failed to connect to database!")
	}

	db.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(&Article{})

	DB = db
}
