package models

type Article struct {
	ID      int    `json:"id" gorm:"primary_key;auto_increment"`
	Title   string `json:"title" gorm:"type:varchar(255);not null;"`
	Content string `json:"content" gorm:"type:text;not null;"`
	Author  string `json:"author" gorm:"type:varchar(100);not null;"`
}
