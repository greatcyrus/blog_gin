FROM golang:latest

ENV GIN_MODE=release \
    DB_USER=blog \
    DB_PASS=0987 \
    DB_HOST=blog-db

WORKDIR /app

COPY . .

RUN go mod download

RUN go build

EXPOSE 8080

CMD ["./blogs"]