package main

import (
	"blogs/controllers"
	"blogs/models"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.Use(gin.Logger())
	r.Use(gin.Recovery())

	models.ConnectDataBase()

	r.POST("/articles", controllers.CreateArticle)
	r.GET("/articles/:id", controllers.GetArticle)
	r.GET("/articles", controllers.GetArticles)

	r.Run()
}
