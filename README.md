# BLOG

This project is built via [Gin](https://github.com/gin-gonic/gin), [gorm](https://github.com/jinzhu/gorm/) and mysql

# HOW TO RUN

- Assume the docker and docker-compose are installed
- Docker version is **19.03.8** and docker-compose version is **1.25.5**
- Run `docker-compose up -d` at the root of the project

# HOW TO RUN TEST OR BUILD DEVELOPMENT ENVIRONMENT

- Assume the mysql database is installed
- MySQL version is **5.7**
- Run `. ./start.sh` at the root of the project
- Then run `go test ./...` or `go test -cover ./...`
